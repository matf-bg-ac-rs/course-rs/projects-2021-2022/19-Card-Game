#include "cardsymbol.hpp"

QString CardSymbol::symbolToString(symbols symbol){
    QString symbolStrings [4]= {"Clubs", "Spades", "Hearts", "Diamonds"};
    return symbolStrings[symbol];
}


QString CardSymbol::symbolToString(CardSymbol symbol){
    QString symbolStrings [4]= {"Clubs", "Spades", "Hearts", "Diamonds"};
    return symbolStrings[symbol.symbol];
}

CardSymbol CardSymbol::stringToSymbol(QString symbol){
    QString symbolStrings [4]= {"Clubs", "Spades", "Hearts", "Diamonds"};
    int idx = -1;
    for(auto i = 0; i < 4; i++){
        if(!symbol.compare(symbolStrings[i])){
            idx = i;
            break;
        }
    }
    if(idx == -1){
        qDebug() << "invalid string to number conversion";
    }
    CardSymbol cn{symbols(idx)};
    return cn;
}

QString CardSymbol::toString(){
    return symbolToString(symbol);
}


bool CardSymbol::operator ==(CardSymbol cs){
    return symbol == cs.symbol;
}

bool CardSymbol::operator !=(CardSymbol cs){
    return symbol != cs.symbol;
}
