#include "gameclient.h"
#include <QTcpSocket>
#include <QDataStream>
#include <QJsonParseError>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>
#include <QJsonArray>

GameClient::GameClient(QObject *parent)
    : QObject(parent)
    , m_clientSocket(new QTcpSocket(this))
    , m_loggedIn(false)
{
    connect(m_clientSocket, &QTcpSocket::connected, this, &GameClient::connected);
    connect(m_clientSocket, &QTcpSocket::disconnected, this, &GameClient::disconnected);
    connect(m_clientSocket, &QTcpSocket::readyRead, this, &GameClient::onReadyRead);
    connect(m_clientSocket, QOverload<QAbstractSocket::SocketError>::of(&QAbstractSocket::error), this, &GameClient::error);
    connect(m_clientSocket, &QTcpSocket::disconnected, this, [this]()->void{m_loggedIn = false;});
}

void GameClient::login(const QString &userName)
{
    if (m_clientSocket->state() == QAbstractSocket::ConnectedState) {
        QDataStream clientStream(m_clientSocket);
        clientStream.setVersion(QDataStream::Qt_5_12);
        QJsonObject message;
        message[QStringLiteral("type")] = QStringLiteral("login");
        message[QStringLiteral("username")] = userName;
        clientStream << QJsonDocument(message).toJson(QJsonDocument::Compact);
    }
}

void GameClient::betRequest(int bet)
{
    QDataStream clientStream(m_clientSocket);
    clientStream.setVersion(QDataStream::Qt_5_12);
    QJsonObject message;
    message[QStringLiteral("type")] = QStringLiteral("bet");
    message[QStringLiteral("bet")] = bet;
    clientStream << QJsonDocument(message).toJson();
};

void GameClient::disconnectFromHost()
{
    qDebug() << "FILE: " << __FILE__ << ", FUNCTION: "
             << __FUNCTION__ << " called";
    m_clientSocket->disconnectFromHost();
}

void GameClient::jsonReceived(const QJsonObject &docObj)
{
    const QJsonValue typeVal = docObj.value(QLatin1String("type"));
    if (typeVal.isNull() || !typeVal.isString())
        return;
    if (typeVal.toString().compare(QLatin1String("login"), Qt::CaseInsensitive) == 0) {
        if (m_loggedIn)
            return;
        const QJsonValue resultVal = docObj.value(QLatin1String("success"));
        if (resultVal.isNull() || !resultVal.isBool()) {
            disconnectFromHost();
            return;
        }
        const bool loginSuccess = resultVal.toBool();
        if (loginSuccess) {
            const QJsonValue idVal = docObj.value(QLatin1String("id"));
            emit loggedIn(idVal.toInt());
            return;
        }
        const QJsonValue reasonVal = docObj.value(QLatin1String("reason"));
        emit loginError(reasonVal.toString());
        disconnectFromHost();
    } else if (typeVal.toString().compare(QLatin1String("bet"), Qt::CaseInsensitive) == 0) {
        const QJsonValue idVal = docObj.value(QLatin1String("id"));
        const QJsonValue betVal = docObj.value(QLatin1String("bet"));
        emit betReceived(idVal.toInt(),betVal.toInt());
   } else if (typeVal.toString().compare(QLatin1String("start"), Qt::CaseInsensitive) == 0) {
        const QJsonValue moneyVal = docObj.value(QLatin1String("money"));
        int money = moneyVal.toInt();
        players.clear();
        QJsonArray playerArray = docObj.value(QLatin1String("players")).toArray();
        for (auto i=0;i<playerArray.size();i++) {
            QJsonObject playerObject = playerArray[i].toObject();
            if (playerObject[QStringLiteral("ai")].toBool()) {
                Player player(playerObject[QStringLiteral("id")].toInt(),money,playerObject[QStringLiteral("name")].toString(),Player::AI);
                players.append(player);
            }
            else {
                Player player(playerObject[QStringLiteral("id")].toInt(),money,playerObject[QStringLiteral("name")].toString(),Player::Human);
                players.append(player);
            }
        }
        emit gameStarted(players,money);
   } else if (typeVal.toString().compare(QLatin1String("fold"), Qt::CaseInsensitive) == 0) {
        const QJsonValue idVal = docObj.value(QLatin1String("id"));
        emit foldReceived(idVal.toInt());
   } else if (typeVal.toString().compare(QLatin1String("newplayer"), Qt::CaseInsensitive) == 0) {
        const QJsonValue idVal = docObj.value(QLatin1String("id"));
        const QJsonValue nameVal = docObj.value(QLatin1String("username"));
        emit playerAdded(idVal.toInt(),nameVal.toString());
   } else if (typeVal.toString().compare(QLatin1String("playerleft"), Qt::CaseInsensitive) == 0) {
        const QJsonValue idVal = docObj.value(QLatin1String("id"));
        emit playerRemoved(idVal.toInt());
   } else if (typeVal.toString().compare(QLatin1String("round"), Qt::CaseInsensitive) == 0) {
        emit roundStarted();
   } else if (typeVal.toString().compare(QLatin1String("cards"), Qt::CaseInsensitive) == 0) {
        const QJsonValue symbol1Val = docObj.value(QLatin1String("symbol1"));
        const QJsonValue number1Val = docObj.value(QLatin1String("number1"));
        const QJsonValue symbol2Val = docObj.value(QLatin1String("symbol2"));
        const QJsonValue number2Val = docObj.value(QLatin1String("number2"));
        emit cardsReceived(symbol1Val.toString(),number1Val.toInt(),symbol2Val.toString(),number2Val.toInt());
   } else if (typeVal.toString().compare(QLatin1String("flop"), Qt::CaseInsensitive) == 0) {
        const QJsonValue symbol1Val = docObj.value(QLatin1String("symbol1"));
        const QJsonValue number1Val = docObj.value(QLatin1String("number1"));
        const QJsonValue symbol2Val = docObj.value(QLatin1String("symbol2"));
        const QJsonValue number2Val = docObj.value(QLatin1String("number2"));
        const QJsonValue symbol3Val = docObj.value(QLatin1String("symbol3"));
        const QJsonValue number3Val = docObj.value(QLatin1String("number3"));
        emit flopReceived(symbol1Val.toString(),number1Val.toInt(),symbol2Val.toString(),number2Val.toInt(),symbol3Val.toString(),number3Val.toInt());
   } else if (typeVal.toString().compare(QLatin1String("turn"), Qt::CaseInsensitive) == 0) {
        const QJsonValue symbolVal = docObj.value(QLatin1String("symbol"));
        const QJsonValue numberVal = docObj.value(QLatin1String("number"));
        emit turnReceived(symbolVal.toString(),numberVal.toInt());
   } else if (typeVal.toString().compare(QLatin1String("river"), Qt::CaseInsensitive) == 0) {
        const QJsonValue symbolVal = docObj.value(QLatin1String("symbol"));
        const QJsonValue numberVal = docObj.value(QLatin1String("number"));
        emit riverReceived(symbolVal.toString(),numberVal.toInt());
   } else if (typeVal.toString().compare(QLatin1String("waiting"), Qt::CaseInsensitive) == 0) {
        const QJsonValue idVal = docObj.value(QLatin1String("id"));
        const QJsonValue betVal = docObj.value(QLatin1String("bet"));
        emit waitingForInput(idVal.toInt(),betVal.toInt());
   } else if (typeVal.toString().compare(QLatin1String("active"), Qt::CaseInsensitive) == 0) {
        const QJsonValue idVal = docObj.value(QLatin1String("id"));
        emit playerToMove(idVal.toInt());
   } else if (typeVal.toString().compare(QLatin1String("change"), Qt::CaseInsensitive) == 0) {
        const QJsonValue idVal = docObj.value(QLatin1String("id"));
        const QJsonValue moneyVal = docObj.value(QLatin1String("money"));
        emit changeMoney(idVal.toInt(),moneyVal.toInt());
   } else if (typeVal.toString().compare(QLatin1String("end"), Qt::CaseInsensitive) == 0) {
        emit gameEnded();
   } else if (typeVal.toString().compare(QLatin1String("show"), Qt::CaseInsensitive) == 0) {
        const QJsonValue idVal = docObj.value(QLatin1String("id"));
        const QJsonValue symbol1Val = docObj.value(QLatin1String("symbol1"));
        const QJsonValue number1Val = docObj.value(QLatin1String("number1"));
        const QJsonValue symbol2Val = docObj.value(QLatin1String("symbol2"));
        const QJsonValue number2Val = docObj.value(QLatin1String("number2"));
        emit showCards(idVal.toInt(),symbol1Val.toString(),number1Val.toInt(),symbol2Val.toString(),number2Val.toInt());
   } else if (typeVal.toString().compare(QLatin1String("winner"), Qt::CaseInsensitive) == 0) {
        const QJsonValue idVal = docObj.value(QLatin1String("id"));
        emit playerWon(idVal.toInt());
   }
}

void GameClient::connectToServer(const QHostAddress &address, quint16 port)
{
    m_clientSocket->connectToHost(address, port);
}

void GameClient::onReadyRead()
{
    QByteArray jsonData;
    QDataStream socketStream(m_clientSocket);
    socketStream.setVersion(QDataStream::Qt_5_12);
    for (;;) {
        socketStream.startTransaction();
        socketStream >> jsonData;
        if (socketStream.commitTransaction()) {
            QJsonParseError parseError;
            const QJsonDocument jsonDoc = QJsonDocument::fromJson(jsonData, &parseError);
            if (parseError.error == QJsonParseError::NoError) {
                if (jsonDoc.isObject())
                    jsonReceived(jsonDoc.object());
            }
        } else {
            break;
        }
    }
}
