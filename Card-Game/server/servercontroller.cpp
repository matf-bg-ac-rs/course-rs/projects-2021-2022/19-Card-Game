#include "ui_servercontroller.h"
#include "gameserver.h"
#include "servercontroller.h"
#include <QMessageBox>
#include <QVector>
#include <QTimer>

#define DEBUG

ServerController::ServerController(QWidget *parent, int startingMoney, int bigBlind)
    : QWidget(parent)
    , bigBlind(bigBlind)
    , startingMoney(startingMoney)
    , ui(new Ui::ServerController)
    , m_gameServer(new GameServer(this))
{
    ui->setupUi(this);

    botCount = 2;

    for (auto i = 2; i < 15; i++) {
        deck.push_back(Card{CardSymbol::Diamonds, i});
        deck.push_back(Card{CardSymbol::Clubs, i});
        deck.push_back(Card{CardSymbol::Hearts, i});
        deck.push_back(Card{CardSymbol::Spades, i});
    }

    smallBlind = bigBlind/2;
    srand(time(0));

    connect(ui->startStopButton, &QPushButton::clicked, this, &ServerController::toggleStartServer);
    connect(ui->startButton, &QPushButton::clicked, this, &ServerController::startGame);
    connect(m_gameServer, &GameServer::logMessage, this, &ServerController::logMessage);
    connect(m_gameServer, &GameServer::playerAdded, this, &ServerController::playerAdded);
    connect(m_gameServer, &GameServer::playerRemoved, this, &ServerController::playerRemoved);
    connect(m_gameServer, &GameServer::betReceived, this, &ServerController::betReceived);
}

ServerController::~ServerController()
{
    delete ui;
}

void ServerController::toggleStartServer()
{
    if (m_gameServer->isListening()) {
        m_gameServer->stopServer();
        ui->startStopButton->setText(tr("Start Server"));
        logMessage(QStringLiteral("Server Stopped"));
    } else {
        if (!m_gameServer->listen(QHostAddress::Any, 1967)) {
            QMessageBox::critical(this, tr("Error"), tr("Unable to start the server"));
            return;
        }
        logMessage(QStringLiteral("Server Started"));
        ui->startStopButton->setText(tr("Stop Server"));
    }
}

void ServerController::closeEvent(QCloseEvent *event)
{
    emit windowClosed();
    event->accept();
}

bool ServerController::isServerStarted() const
{
    return m_gameServer->isListening();
}

void ServerController::logMessage(const QString &msg)
{
    ui->logEditor->appendPlainText(msg + QLatin1Char('\n'));
}

void ServerController::startGame(unsigned botCount)
{
    for (auto i = 0; i < botCount ; i++ ) {
        players.push_back(Player{nextID++, startingMoney, botNames[rand() % botNames.size()], Player::AI});
        m_gameServer->incrementId();
#ifdef DEBUG
   qDebug() << "Added bot with ID:" << nextID - 1;
#endif
    }

    bigBlindIndex = rand() % players.size();
    smallBlindIndex = bigBlindIndex - 1;
    smallBlindIndex = smallBlindIndex < 0 ? players.size() - 1 : smallBlindIndex;
    for (Player &p : players) {
        p.setMoney(startingMoney);
        p.getCards().clear();
    }
    QVector<Player> qplayers = QVector<Player>::fromStdVector(players);
    m_gameServer->startGame(qplayers,startingMoney);
    startRound();
}

void ServerController::startRound()
{

#ifdef DEBUG
    qDebug() << "Round starting";
#endif

    //Logika koja je potrebna pre pocetka partije
    stage = Preflop;

    //pomera se big i small blind
    smallBlindIndex = bigBlindIndex;
    bigBlindIndex = (bigBlindIndex + 1) % players.size();

    //postavlja se pocetna opklada na bigBlind
    betPerPlayer = bigBlind;

    //opklade za sve igrace se resetuju na 0, a na stolu nema novca na pocetku
    resetBets();
    pot = 0;
    m_gameServer->startRound();

    setAllPlayersDeciding();
    int bet;
    //bigBlind i smallBlind su prisiljeni da se klade
    if (players[bigBlindIndex].getMoney() < bigBlind) {
        bet = players[bigBlindIndex].makeBet(players[bigBlindIndex].getMoney());
        pot += bet;
        players[bigBlindIndex].setStatus(Player::AllIn);
    } else {
        bet = players[bigBlindIndex].makeBet(bigBlind);
        pot+= bet;
    }
     m_gameServer->betMade(players[bigBlindIndex].getID(), bet);
#ifdef DEBUG
        qDebug() << "Player ID:" << players[bigBlindIndex].getID() << "paid big blind:" << bet;
#endif


    if (players[smallBlindIndex].getMoney() < smallBlind) {
        bet = players[smallBlindIndex].makeBet(players[smallBlindIndex].getMoney());
        pot+= bet;
        players[smallBlindIndex].setStatus(Player::AllIn);
    } else{
        bet = players[smallBlindIndex].makeBet(smallBlind);
        pot += bet;
    }
#ifdef DEBUG
    qDebug() << "Player ID:" << players[smallBlindIndex].getID() << "paid small blind:" << bet;
#endif
    m_gameServer->betMade(players[smallBlindIndex].getID(), bet);

    playStage();
}

void ServerController::playerAdded(QString username)
{
    Player newPlayer(nextID++, startingMoney, username);
    qDebug() << "Added player with ID:" << nextID-1;
    players.push_back(newPlayer);
    emit playerCountChanged(players.size());
}

void ServerController::playerRemoved(int id)
{
#ifdef DEBUG
    qDebug() << "Removing player with ID:" << id;
#endif
    //Ukoliko je igrac izbacen, izbacuje se tek na kraju runde, da bi moglo da se odigra do kraja bez promena
    int foundID = findPlayer(id);
    if(foundID != -1) {
        Player &removedPlayer = players[foundID];
        //Njegov novac se postavlja na 0 i pokupice ga funkicja removePlayersWithoutMoney na kraju runde
        removedPlayer.changeMoney(-removedPlayer.getMoney());

        //Ukoliko je bio na potezu, odigrace -1 - sto ce biti Fold
        if(*playerToMove == removedPlayer){
            betReceived(id, -1);
        }
        //U suprotnom prelazi u stanje Folded i bice ignorisan do kraja runde
        else{
            removedPlayer.setStatus(Player::Folded);
        }
    }
    else {
#ifdef DEBUG
        qDebug() << "Failed to remove player with ID:" << id << ", they were not found in players[]";
#endif
    }
}

void ServerController::betReceived(int id, int bet)
{
#ifdef DEBUG
    qDebug() << "Player ID:" << id << "Made bet:" << bet;
#endif

    if(!betIsValid(id, bet)) return;

    waitingForBets = false;

    //Ovo je bitno za slucaj Raise
    //Ako igrac A ulozi 10, a igrac B posle njega 20, onda A treba jos 10
    int totalBet = playerToMove->getBet() + bet;


    //All in slucaj
    if(bet == playerToMove->getMoney()){
        betPerPlayer = totalBet > betPerPlayer ? totalBet : betPerPlayer;
#ifdef DEBUG
        qDebug() << "Player ID:" << id << "is all in";
#endif
        setCheckedPlayersDeciding();
        pot += playerToMove->makeBet(bet);
        playerToMove->setStatus(Player::AllIn);
        m_gameServer->betMade(id, bet);
    }
    //Call slucaj
    else if(totalBet == betPerPlayer){
#ifdef DEBUG
        qDebug() << "Player ID:" << id << "called";
#endif
        pot += playerToMove->makeBet(bet);
        playerToMove->setStatus(Player::Checked);
        m_gameServer->betMade(id, bet);
    }
    //Raise slucaj
    else if(totalBet > betPerPlayer){
#ifdef DEBUG
        qDebug() << "Player ID:" << id << "raised";
#endif
        //Menja se nova opklada
        betPerPlayer = totalBet;
        //Svi igraci koji su jos u rundi se ponovo postavljaju na Deciding jer treba da odluci da li da prate novu opkladu
        setCheckedPlayersDeciding();

        pot += playerToMove->makeBet(bet);
        playerToMove->setStatus(Player::Checked);
        m_gameServer->betMade(id, bet);
    }
    //Fold slucaj
    else{
#ifdef DEBUG
        qDebug() << "Player ID:" << id << " folded";
#endif
        playerToMove->setStatus(Player::Folded);
        m_gameServer->playerFolded(id);

        //Ako je ostao samo 1 igrac, zavrsava se krug i on je pobednik
        if(notFoldedCount() == 1){
#ifdef DEBUG
            qDebug() << "Only one player has not folded so he wins";
#endif
            stage = River;
            endStage();
            return;
        }
    }


    //Ukoliko su svi igraci u stanju Checked ili Folded, moze da se zavrsi krug
    if(allPlayersFinishedTurn()){
        //Ovde se ulazi kada je ostao samo jedan igrac koji nije u stanju All In
        //Nema potrebe da se vise igra, nego se podele preostale karte na sto i bira se pobednik
        if(activePlayerCount() <= 1){
#ifdef DEBUG
        qDebug() << "Only one player is not all in so the round ends now";
#endif
            dealRemainingCards();
            return;

        }
        endStage();
    }
    else{
        //U suprotnom, postavlja se sledeci igrac na potezu i server nastavlja da ceka
        //Preskacu se Folded igraci
        do{
            nextPlayer();
        }while(playerToMove->getStatus() != Player::Deciding);

        proccessBets();
    }

}

bool ServerController::betIsValid(int id, int bet){

    //Zastita od mogucnosti da se ovaj signal poslao kad ne treba
    if(!waitingForBets) {
#ifdef DEBUG
        qDebug() << "Bet sent at the wrong time";
#endif
        return false;
    }

    //Ako dati igrac nije na potezu, njegova opklada se ignorise
    if(playerToMove->getID() != id) {
#ifdef DEBUG
        qDebug() << "Wrong player sent bet - player to move is ID:" << playerToMove->getID();
#endif
        return false;
    }

    //Ako igrac nema dovoljno novca, njegova opklada se ignorise
    //Moguca izmena: staviti bet = playerToMove->getMoney() - ako igrac pokusa da ulozi preko max novca, smatra se da ulaze max, tj All in
    if(playerToMove->getMoney() < bet) {
#ifdef DEBUG
        qDebug() << "Player ID:" << id << "tried to bet:" << bet << "but he only has " << playerToMove->getMoney() << "money";
#endif
        return false;
    }

    //Ignorisemo igraca u stanu Folded
    if(playerToMove->getStatus() == Player::Folded)
    {
#ifdef DEBUG
        qDebug() << "Player ID:" << id << "folded, but tried to make a bet";
#endif
        return false;
    }

    if(playerToMove->getStatus() == Player::AllIn)
    {
#ifdef DEBUG
        qDebug() << "Player ID:" << id << "is all in, but tried to make a bet";
#endif
        return false;
    }

    return true;
}

int ServerController::activePlayerCount(){
    int count = 0;
    for(auto &p : players){
        if(p.getStatus() != Player::AllIn && p.getStatus() != Player::Folded){
            count++;
        }
    }
    return count;
}

void ServerController::dealRemainingCards(){
    if(stage == Preflop){
        stage = Flop;
        dealCards();
    }
    if(stage == Flop){
        stage = Turn;
        dealCards();
    }
    if(stage == Turn){
        stage = River;
        dealCards();
    }
    endStage();
}

unsigned ServerController::getPlayerCount(){
    return players.size();
}

void ServerController::playStage(){
    dealCards();

    for (Player &player : players) {
        player.resetRaised();
    }

    setCheckedPlayersDeciding();


    playerToMoveIndex = (bigBlindIndex + 1) % players.size();
    playerToMove = &players[playerToMoveIndex];

    while(playerToMove->getStatus() != Player::Deciding){
        nextPlayer();
    }
    proccessBets();

}

void ServerController::dealCards(){
    switch (stage) {
    case Preflop:
        for(auto it = players.begin(); it != players.end(); it++){
            std::vector<Card> &playerCards = it->getCards();
            moveRandomCards(deck, playerCards, 2);
            Card &card1 = playerCards[0];
            Card &card2 = playerCards[1];

            //Verovatno bi bilo jednostavnije da zapis funkcija bude sendCards(int id, Card c1, Card c2) ili tako nesto
            //Ovo izgleda malo ruzno ali radi, pa nije prioritet
            m_gameServer->sendCards(it->getID(),
                                    card1.getSymbol().toString(), card1.getNumber().toInt(),
                                    card2.getSymbol().toString(), card2.getNumber().toInt());
        }
        break;
    case Flop:
        //slican pristup kao prethodni slucaj - prvo se interno dodaju karte pa se opali signal da obavesti klijente
        moveRandomCards(deck, communalCards, 3);
        m_gameServer->sendFlop(communalCards[0].getSymbol().toString(), communalCards[0].getNumber().toInt(),
                communalCards[1].getSymbol().toString(), communalCards[1].getNumber().toInt(),
                communalCards[2].getSymbol().toString(), communalCards[2].getNumber().toInt());

        break;
    case Turn:
        moveRandomCards(deck, communalCards);
        m_gameServer->sendTurn(communalCards[3].getSymbol().toString(), communalCards[3].getNumber().toInt());
        break;
    case River:
        moveRandomCards(deck, communalCards);
        m_gameServer->sendRiver(communalCards[4].getSymbol().toString(), communalCards[4].getNumber().toInt());
        break;
    }
}

void ServerController::proccessBets(){
    waitingForBets = true;
    int betToCall = betPerPlayer - playerToMove->getBet();
#ifdef DEBUG
    qDebug() << "Expecting bet:" << betToCall << "From Player ID:" << playerToMove->getID();
#endif
    if(playerToMove->getPlayerType() == Player::Human){
        m_gameServer->waitingForInput(playerToMove->getID(), betToCall);
    }
    else{
        int response;
        do{
            response = playerToMove->getResponse(communalCards, betToCall, notFoldedCount());

        }while(!betIsValid(playerToMove->getID(), response));
        betReceived(playerToMove->getID(), response);
    }
}

void ServerController:: endStage(){

#ifdef DEBUG
    qDebug() << "Stage is over";
#endif
    //River je poslednji stage
    if(stage != River){
        //menja stage i pocinje ga na serveru
        //na pocetku playStage() se emituje signal pa ce igraci biti obavesteni
        stage = Stages(stage + 1);
        playStage();
        return;
    }

    //Ukoliko se u ovu funkciju stigne ranije, communal cards moze biti prazan vektor, a decideWinner ocekuje da ce imati 5 karti
    int winner = notFoldedCount() == 1 ? findNotFoldedPlayer() : decideWinner();

    if(players[winner].getStatus() != Player::AllIn){
        players[winner].changeMoney(pot);
        m_gameServer->changeMoney(players[winner].getID(), pot);
    } else {
        int winnerBet = players[winner].getBet();
        int winnerPot = 0;
        for(auto &p : players){
            if(p == players[winner]) continue;

            int overbet = p.getBet() - winnerBet;
            if(overbet > 0){
                p.changeMoney(overbet);
                m_gameServer->changeMoney(p.getID(), overbet);
            }

            winnerPot += p.getBet() < winnerBet ? p.getBet() : winnerBet;
        }
        players[winner].changeMoney(winnerPot);
        m_gameServer->changeMoney(players[winner].getID(), winnerPot);
    }

#ifdef DEBUG
    writePlayers();
    writeCommunalCards();
    qDebug() << "Money in the pot was: " << pot;
    qDebug() << "Winner is player " << players[winner].getID();
#endif

    m_gameServer->playerWon(players[winner].getID());
    if(notFoldedCount() > 1){

        for(auto &p : players){
            if(p.getStatus() == Player::Folded) continue;
            auto &pCards = p.getCards();
            m_gameServer->showCards(p.getID(),
                                    pCards[0].getSymbol().toString(), pCards[0].getNumber().toInt(),
                    pCards[1].getSymbol().toString(), pCards[1].getNumber().toInt()
                    );
        }
    }

    returnCardsToDeck();

    //Ovo obavezno pozvati poslednje, jer moze da unisti Player objekte
    removePlayersWithoutMoney();

    //Na kraju runde se prikazu karte drugih igraca.
    //Problem je sto se odmah predje u sledecu rundu i karte se sklone sa stola
    //Potrebna je pauza pre samog pocetka runde da bi bilo vremene da se karte vide

    QTimer::singleShot(2500, this, &ServerController::nextRound);
}

void ServerController::nextRound(){
    //Uslov za nastavak partije
    //Ovaj uslov mozemo da se dogovorimo, ali za sada ce partija da se nastavi dok god postoji vise od 1 igraca
    if(players.size() > 1){
        startRound();
    } else {
        players.clear();
        m_gameServer->gameEnded();
    }
}

void ServerController::removePlayersWithoutMoney(){
    for (auto i = 0; i < players.size(); i++) {
        if (players[i].getMoney() == 0) {
#ifdef DEBUG
            qDebug() << "Removing player ID:" << players[i].getID();
#endif
            removePlayer(players[i--]);
        }
    }
}

int ServerController::findNotFoldedPlayer(){
    for(auto i = 0; i < players.size(); i++){
        if(players[i].getStatus() != Player::Folded){
            return i;
        }
    }
    return -1;
}

void ServerController::returnCardsToDeck(){
    moveRandomCards(communalCards, deck, communalCards.size());
    for (auto it = players.begin(); it != players.end(); it++) {
        moveRandomCards(it->getCards(), deck, it->getCards().size());
    }
}

int ServerController::notFoldedCount(){
    int count = 0;
    for(auto &p : players){
        if(p.getStatus() != Player::Folded){
            count++;
        }
    }
    return count;
}

//BUG: Trenutno se u funkciji ne razlikuje, na primer, ko ima jaci par. Par trojki i par kraljeva je potpuno isti, a pobednik se bira nasumicno
int ServerController::decideWinner() {

    //nalazi se prvi igrac koji nije u stanju Folded
    int idx = -1;

    for(auto i = 0; i < players.size(); i++){
        if(players[i].getStatus() != Player::Folded){
            idx = i;
            break;
        }
    }
    if(idx == -1){
        qDebug() << "All players folded - wtf";
    }

    //Ako je prvi igrac koji nije Folded zapravo poslednji igrac, onda je on pobednik
    if(idx == players.size() -1){
        return idx;
    }
    CardHand ch(communalCards, players[idx].getCards());
    CardHand::Hands bestHandEvaluation = ch.bestHandEvaluation;
    std::vector<Card> &bestHand = ch.strongestHand;
    for(ulong i = idx + 1; i < players.size(); i++){
        if(players[i].getStatus() == Player::Folded) continue;
        ch = CardHand(communalCards, players[i].getCards());
        if(bestHandEvaluation < ch.bestHandEvaluation){
            bestHandEvaluation = ch.bestHandEvaluation;
            idx = i;
            bestHand = ch.strongestHand;
        }
    }

    return idx;
}

void ServerController::setAllPlayersDeciding() {
    for (auto &p : players) {
        p.setStatus(Player::Deciding);
    }
}

void ServerController::resetBets() {
    for (auto &p : players) {
        p.resetBet();
    }
}

void ServerController::setCheckedPlayersDeciding() {
    for (auto it = players.begin(); it != players.end(); it++) {
        if (it->getStatus() == Player::Checked) {
            it->setStatus(Player::Deciding);
        }
    }
}
void ServerController::nextPlayer(){
    playerToMoveIndex = (playerToMoveIndex + 1) % players.size();
    playerToMove = &players[playerToMoveIndex];
}

void ServerController::removePlayer(Player &player) {
    int id = player.getID();
    int idx = findPlayer(player);

    players.erase(players.begin() + idx);
    m_gameServer->removePlayer(id);

   //Mora da se uskladi bigBlindIndex i smallBlindIndex sa brisanjem igraca
    bigBlindIndex = bigBlindIndex % players.size();

    smallBlind = bigBlindIndex > 0 ? bigBlindIndex - 1 : players.size() - 1;
}

void ServerController::writePlayers(){
    for(auto &p: players){
        qDebug() << p.toString();
    }
}

void ServerController::writeCommunalCards(){
    for(auto &c: communalCards){
        qDebug() << c.toString();
    }
}

bool ServerController::allPlayersFinishedTurn() {
    for (auto &p : players) {
        if(!p.finishedTurn()){
            return false;
        }
    }
    return true;
}

void ServerController::moveCard(std::vector<Card> &origin, std::vector<Card> &dest, int idx){
    dest.push_back(origin[idx]);
    origin.erase(origin.begin() + idx);
}

void ServerController::moveRandomCards(std::vector<Card> &origin, std::vector<Card> &dest, int count){
    for (auto i = 0; i < count; i++){
        moveCard(origin, dest, rand() % origin.size());
    }
}

int ServerController::findPlayer(Player &player){
    for(size_t i = 0; i < players.size(); i++){
        if(players[i] == player) return i;
    }
    return -1;
}

int ServerController::findPlayer(int id){
    for(size_t i = 0; i < players.size(); i++){
        if(players[i].getID() == id) return i;
    }
    return -1;
}
