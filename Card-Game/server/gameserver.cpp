#include "gameserver.h"
#include "serverworker.h"
#include <QThread>
#include <functional>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>
#include <QJsonArray>
#include <QTimer>
#include <QRandomGenerator>

GameServer::GameServer(QObject *parent)
    : QTcpServer(parent)
{}

void GameServer::incrementId()
{
    nextId++;
}

void GameServer::incomingConnection(qintptr socketDescriptor)
{
    if (m_gameStarted)
        return;
    ServerWorker *worker = new ServerWorker(this);
    if (!worker->setSocketDescriptor(socketDescriptor)) {
        worker->deleteLater();
        return;
    }
    connect(worker, &ServerWorker::disconnectedFromClient, this, std::bind(&GameServer::userDisconnected, this, worker));
    connect(worker, &ServerWorker::error, this, std::bind(&GameServer::userError, this, worker));
    connect(worker, &ServerWorker::jsonReceived, this, std::bind(&GameServer::jsonReceived, this, worker, std::placeholders::_1));
    connect(worker, &ServerWorker::logMessage, this, &GameServer::logMessage);
    m_clients.append(worker);
    emit logMessage(QStringLiteral("New client Connected"));
}
void GameServer::sendJson(ServerWorker *destination, const QJsonObject &message)
{
    Q_ASSERT(destination);
    destination->sendJson(message);
}
void GameServer::sendJson(int id, const QJsonObject &message)
{
    for (ServerWorker *worker : qAsConst(m_clients)) {
        if (worker->getPlayerId() == id) {
            sendJson(worker,message);
            break;
        }
    }
}
void GameServer::broadcast(const QJsonObject &message, ServerWorker *exclude)
{
    for (ServerWorker *worker : qAsConst(m_clients)) {
        Q_ASSERT(worker);
        if (worker == exclude)
            continue;
        sendJson(worker, message);
    }
}
void GameServer::broadcast(const QJsonObject &message, int id)
{
    for (ServerWorker *worker : qAsConst(m_clients))
        if (worker->getPlayerId() != id)
            sendJson(worker, message);
}

void GameServer::jsonReceived(ServerWorker *sender, const QJsonObject &doc)
{
    Q_ASSERT(sender);
    emit logMessage(QLatin1String("JSON received ") + QString::fromUtf8(QJsonDocument(doc).toJson()));
    if (sender->userName().isEmpty())
        return jsonFromLoggedOut(sender, doc);
    jsonFromLoggedIn(sender, doc);
}

void GameServer::userDisconnected(ServerWorker *sender)
{
    m_clients.removeAll(sender);
    if (sender->getPlayerId() >= 0) {
        QJsonObject disconnectedMessage;
        disconnectedMessage[QStringLiteral("type")] = QStringLiteral("userdisconnected");
        disconnectedMessage[QStringLiteral("username")] = sender->userName();
        broadcast(disconnectedMessage, nullptr);
        //emit logMessage(userName + QLatin1String(" disconnected"));
        auto id = sender->getPlayerId();
        sender->setPlayerId(-1);
        m_players.removeAll(sender->userName());
        m_ids.removeAll(id);
        emit playerRemoved(id);
    }
    sender->deleteLater();
}

void GameServer::userError(ServerWorker *sender)
{
    Q_UNUSED(sender)
    emit logMessage(QLatin1String("Error from ") + sender->userName());
}

void GameServer::stopServer()
{
    for (ServerWorker *worker : qAsConst(m_clients)) {
        worker->disconnectFromClient();
    }
    close();
}

void GameServer::jsonFromLoggedOut(ServerWorker *sender, const QJsonObject &docObj)
{
    Q_ASSERT(sender);
    const QJsonValue typeVal = docObj.value(QLatin1String("type"));
    if (typeVal.isNull() || !typeVal.isString())
        return;
    if (typeVal.toString().compare(QLatin1String("login"), Qt::CaseInsensitive) != 0)
        return;
    const QJsonValue usernameVal = docObj.value(QLatin1String("username"));
    if (usernameVal.isNull() || !usernameVal.isString())
        return;
    const QString newUserName = usernameVal.toString().simplified();
    if (newUserName.isEmpty())
        return;
    for (ServerWorker *worker : qAsConst(m_clients)) {
        if (worker == sender)
            continue;
    }
    sender->setUserName(newUserName);
    sender->setPlayerId(nextId);
    QJsonObject successMessage;
    successMessage[QStringLiteral("type")] = QStringLiteral("login");
    successMessage[QStringLiteral("success")] = true;
    successMessage[QStringLiteral("id")] = nextId;
    sendJson(sender, successMessage);
    emit playerAdded(newUserName);
    m_players.append(newUserName);
    m_ids.append(nextId);
    nextId++;
}

void GameServer::jsonFromLoggedIn(ServerWorker *sender, const QJsonObject &docObj)
{
    Q_ASSERT(sender);
    const QJsonValue typeVal = docObj.value(QLatin1String("type"));
    if (typeVal.isNull() || !typeVal.isString())
        return;
    else if (typeVal.toString().compare(QLatin1String("bet"), Qt::CaseInsensitive) == 0) {
        emit betReceived(sender->getPlayerId(),docObj.value(QLatin1String("bet")).toInt());
    }
}

void GameServer::startGame(QVector<Player>& players, int startingMoney)
{
    m_gameStarted=1;
    QJsonObject message;
    message[QStringLiteral("type")] = QStringLiteral("start");
    QJsonArray playerArray;
    for (auto i=0;i<players.length();i++) {
        QJsonObject playerObject;
        playerObject[QStringLiteral("id")] = players[i].getID();
        playerObject[QStringLiteral("name")] = players[i].getUsername();
        playerObject[QStringLiteral("ai")] = (players[i].getPlayerType()==Player::AI);
        playerArray.append(playerObject);
    }
    message[QStringLiteral("players")] = playerArray;
    message[QStringLiteral("money")] = startingMoney;
    broadcast(message, nullptr);
}
void GameServer::startRound()
{
    QJsonObject message;
    message[QStringLiteral("type")] = QStringLiteral("round");
    broadcast(message, nullptr);
}
void GameServer::betMade(int id, int bet)
{
    QJsonObject message;
    message[QStringLiteral("type")] = QStringLiteral("bet");
    message[QStringLiteral("id")] = id;
    message[QStringLiteral("bet")] = bet;
    broadcast(message, nullptr);
}
void GameServer::playerFolded(int id)
{
    QJsonObject message;
    message[QStringLiteral("type")] = QStringLiteral("fold");
    message[QStringLiteral("id")] = id;
    broadcast(message, nullptr);
}
void GameServer::sendCards(int id, QString symbol1, int number1, QString symbol2, int number2)
{
    QJsonObject message;
    message[QStringLiteral("type")] = QStringLiteral("cards");
    message[QStringLiteral("symbol1")] = symbol1;
    message[QStringLiteral("number1")] = number1;
    message[QStringLiteral("symbol2")] = symbol2;
    message[QStringLiteral("number2")] = number2;
    sendJson(id,message);
}
void GameServer::sendFlop(QString symbol1, int number1, QString symbol2, int number2, QString symbol3, int number3)
{
    QJsonObject message;
    message[QStringLiteral("type")] = QStringLiteral("flop");
    message[QStringLiteral("symbol1")] = symbol1;
    message[QStringLiteral("number1")] = number1;
    message[QStringLiteral("symbol2")] = symbol2;
    message[QStringLiteral("number2")] = number2;
    message[QStringLiteral("symbol3")] = symbol3;
    message[QStringLiteral("number3")] = number3;
    broadcast(message, nullptr);
};
void GameServer::sendTurn(QString symbol, int number)
{
    QJsonObject message;
    message[QStringLiteral("type")] = QStringLiteral("turn");
    message[QStringLiteral("symbol")] = symbol;
    message[QStringLiteral("number")] = number;
    broadcast(message, nullptr);
};
void GameServer::sendRiver(QString symbol, int number)
{
    QJsonObject message;
    message[QStringLiteral("type")] = QStringLiteral("river");
    message[QStringLiteral("symbol")] = symbol;
    message[QStringLiteral("number")] = number;
    broadcast(message, nullptr);
};
void GameServer::waitingForInput(int id, int bet)
{
    QJsonObject message;
    message[QStringLiteral("type")] = QStringLiteral("waiting");
    message[QStringLiteral("id")] = id;
    message[QStringLiteral("bet")] = bet;
    broadcast(message, nullptr);
};
void GameServer::playerToMove(int id)
{
    QJsonObject message;
    message[QStringLiteral("type")] = QStringLiteral("active");
    message[QStringLiteral("id")] = id;
    broadcast(message, nullptr);
};
void GameServer::changeMoney(int id, int money)
{
    QJsonObject message;
    message[QStringLiteral("type")] = QStringLiteral("change");
    message[QStringLiteral("id")] = id;
    message[QStringLiteral("money")] = money;
    broadcast(message, nullptr);
};
void GameServer::gameEnded()
{
    QJsonObject message;
    message[QStringLiteral("type")] = QStringLiteral("end");
    broadcast(message, nullptr);
};
void GameServer::playerWon(int id)
{
    QJsonObject message;
    message[QStringLiteral("type")] = QStringLiteral("winner");
    message[QStringLiteral("id")] = id;
    broadcast(message, nullptr);
};
void GameServer::addPlayer(int id, QString username)
{
    QJsonObject message;
    message[QStringLiteral("type")] = QStringLiteral("newplayer");
    message[QStringLiteral("id")] = id;
    message[QStringLiteral("username")] = username;
    broadcast(message, nullptr);
};
void GameServer::removePlayer(int id)
{
    for (ServerWorker *worker : qAsConst(m_clients)) {
        if (worker->getPlayerId() == id) {
            m_players.removeAll(worker->userName());
            m_ids.removeAll(id);
            worker->setPlayerId(-1);
            break;
        }
    }
    QJsonObject message;
    message[QStringLiteral("type")] = QStringLiteral("playerleft");
    message[QStringLiteral("id")] = id;
    broadcast(message, nullptr);
};
void GameServer::showCards(int id, QString symbol1, int number1, QString symbol2, int number2)
{
    QJsonObject message;
    message[QStringLiteral("type")] = QStringLiteral("show");
    message[QStringLiteral("id")] = id;
    message[QStringLiteral("symbol1")] = symbol1;
    message[QStringLiteral("number1")] = number1;
    message[QStringLiteral("symbol2")] = symbol2;
    message[QStringLiteral("number2")] = number2;
    broadcast(message,id);
};
