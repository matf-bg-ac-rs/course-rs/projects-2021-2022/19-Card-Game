#include "soundmanager.h"

SoundManager::SoundManager(unsigned initialVolume)
{
    playerJoined.setSource(QUrl("qrc:/sound/366102__original-sound__confirmation-upward.wav"));
    playerLeft.setSource(QUrl("qrc:/sound/246420__waveplaysfx__media-game-sound-sfx-short-generic-menu1.wav"));
    gameStarted.setSource(QUrl("qrc:/sound/256455__deleted-user-4772965__mouse-click.wav"));
    playerWon.setSource(QUrl("qrc:/sound/145434__soughtaftersounds__old-music-box-1.wav"));
    showCards.setSource(QUrl("qrc:/sound/277033__headphaze__ui-completed-status-alert-notification-sfx001.wav"));
    cardsChanged.setSource(QUrl("qrc:/sound/240776__f4ngy__card-flip.wav"));
    playerCountChanged.setSource(QUrl("qrc:/sound/26777__junggle__btn402.wav"));

    setVolume(initialVolume);
}

void SoundManager::setVolume(unsigned newVolume) {
    qreal soundVolume = newVolume / 100.0;
    soundVolume = soundVolume * soundVolume;

    playerJoined.setVolume(soundVolume);
    playerLeft.setVolume(soundVolume);
    gameStarted.setVolume(soundVolume);
    playerWon.setVolume(soundVolume);
    showCards.setVolume(soundVolume);
    cardsChanged.setVolume(soundVolume);
    playerCountChanged.setVolume(soundVolume);
}

void SoundManager::playPlayerJoined()
{
    playerJoined.play();
}

void SoundManager::playPlayerLeft()
{
    playerLeft.play();
}

void SoundManager::playGameStarted()
{
    gameStarted.play();
}

void SoundManager::playPlayerWon()
{
    playerWon.play();
}

void SoundManager::playShowCards()
{
    showCards.play();
}

void SoundManager::playCardsChanged()
{
    cardsChanged.play();
}

void SoundManager::playPlayerCountChanged() {
    playerCountChanged.play();
}
