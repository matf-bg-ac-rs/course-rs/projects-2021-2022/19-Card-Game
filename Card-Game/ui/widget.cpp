#include "widget.h"
#include "ui_widget.h"

Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);

    QFile styleFile( ":/widget.qss" );
    styleFile.open( QFile::ReadOnly );
    this->setStyleSheet( QString::fromLatin1( styleFile.readAll() ) );
    styleFile.close();

    ui->stackedWidget->setCurrentWidget(ui->mainMenuWidget);

    ui->playerCountPicker->setMaximum(maxPlayers);
    ui->playerCountPicker->setMinimum(minPlayers);

    ui->nameLineEdit->setText(settings.value(QString("Name"), QString("Player")).toString());

    ui->musicSlider->setValue(settings.value(QString("Music Volume"), 50).toInt());
    ui->musicSlider->setHidden(true);
    ui->musicVolume->setHidden(true);

    ui->cardHeightEdit->setMinimum(minCardHeight);
    ui->cardHeightEdit->setMaximum(maxCardHeight);
    cardHeight = settings.value(QString("Card Height"), cardHeight).toInt();
    ui->cardHeightEdit->setValue(cardHeight);
    CardLayout::setCardHeight(cardHeight);

    auto soundVolume = settings.value(QString("Sound Volume"), 50).toInt();
    ui->soundSlider->setValue(soundVolume);
    sm.setVolume(soundVolume);

    ui->infoLabel->setStyleSheet("font-size: 12pt; margin: 10pt; ");
    ui->infoLabel->setText("");
    ui->serverPlayerCountLabel->setHidden(true);

    int fontID = QFontDatabase::addApplicationFont(":/font/CoffeetinInitials-YXJ2.ttf");
    if(fontID != -1) {
        QString familiy = QFontDatabase::applicationFontFamilies(fontID)[0];

        QFont font(familiy);
        ui->title->setFont(font);
        ui->title->setStyleSheet("font-size: 40pt; color: black;");
        ui->title2->setFont(font);
        ui->title2->setStyleSheet("font-size: 40pt; color: black;");
        ui->title3->setFont(font);
        ui->title3->setStyleSheet("font-size: 40pt; color: black;");
    }
    else {
        qDebug() << "File: " << __FILE__ << ", line: " << __LINE__ << ", Font for title could not be loaded.";
    }

    blindValidator = new QIntValidator(0, INT_MAX);
    ui->bigBlindEdit->setValidator(blindValidator);

    moneyValidator = new QIntValidator(0, INT_MAX);
    ui->initialMoneyEdit->setValidator(moneyValidator);

    initializeQPixmaps();
    CardLayout::setCardImages(blankCard, invisCard, cardImages);
    CardLayout::setCardHeight(cardHeight);
    initializeLayouts();
    showLayouts();

    connect(ui->betInput, &BetInput::bet, this, &Widget::returnBetSlot);
    connect(this, &Widget::requestBetFromBetInput, ui->betInput, &BetInput::requestBet);

#ifdef DEBUG
    btn = new QPushButton(" * Create Client * ");
    btn->setStyleSheet("font-size: 9pt;");
    connect(btn, &QPushButton::clicked, this, &Widget::createClient);
    ui->horizontalLayout_4->addWidget(btn);
#endif
}

void Widget::deleteServer() {
    if(sc != nullptr) {
        if(sc->isServerStarted()) {
            sc->toggleStartServer();
        }

        delete sc;
    }
    sc = nullptr;
}

Widget::~Widget()
{
    deleteServer();

    if(moneyValidator != nullptr) delete moneyValidator;
    if(blindValidator != nullptr) delete blindValidator;

    if(tableLayout != nullptr) delete tableLayout;
    if(localPlayer != nullptr) delete localPlayer;
    qDeleteAll(playerLayouts);

    if(blankCard != nullptr) delete blankCard;
    if(invisCard != nullptr) delete invisCard;
    if(jokerCard != nullptr) delete jokerCard;

#ifdef DEBUG
    qDeleteAll(ccs);
    if(btn != nullptr) delete btn;
#endif

    delete ui;
}

unsigned Widget::getIndexOfId(int id) {
    unsigned ind;
    for(ind = 0; ind < playerLayouts.size(); ind++) {
        if(playerLayouts[ind]->getId() == id) return ind;
    }

    return playerLayouts.size();
}

void Widget::showMessageBox(QString title, QString text) {
    QMessageBox msgBox;
    msgBox.setWindowTitle(title);
    msgBox.setText(text);
    QSpacerItem* horizontalSpacer = new QSpacerItem(200, 50, QSizePolicy::Minimum, QSizePolicy::Expanding);
    QGridLayout* layout = (QGridLayout*)msgBox.layout();
    layout->addItem(horizontalSpacer, layout->rowCount(), 0, 1, layout->columnCount());
    msgBox.exec();
}

void Widget::messagePlayer(QString msg)
{
    auto label = ui->infoLabel;
    QString text = label->text().trimmed();
    QString seperator(" | ");

    if(msg.size() == 0 || text.size() == 0) {
        text = msg;
    } else if(text.contains(seperator.trimmed()) == false) {
        text = text + seperator + msg;
    } else {
        text = text.split(seperator)[1];
        text = text + seperator + msg;
    }

    label->setText(text);
}

#ifdef DEBUG
void Widget::createClient() {
    auto cc = new ClientController();
    cc->show();
    cc->minimizeGUI();
    cc->move(QApplication::desktop()->screen()->rect().topLeft());
    ccs.push_back(cc);
}
#endif

void Widget::initializeLayouts() {
    tableLayout = new TableLayout(0);

    PlayerLayout *p = new PlayerLayout(true, ui->nameLineEdit->text(), 0);
    playerLayouts.push_back(p);

    for(unsigned i = 1; i < maxPlayers; i++) {
        p = new PlayerLayout(false, QString("Bot"), 0);
        playerLayouts.push_back(p);
        playerLayouts[i]->setVisible(false);
    }
}

void Widget::initializeQPixmaps() {
    QString cardFolderPath(":/images/cards/"), cardPath;
    QString symbols[4] = {"club", "spade", "heart", "diamond"};
    QString numbers[13] = {"-1", "-2", "-3", "-4", "-5", "-6", "-7",
                           "-8", "-9", "-10", "-12", "-13", "-14"};

    cardPath = cardFolderPath;
    cardPath += "back.png";
    blankCard = new QPixmap(cardPath);

    cardPath = cardFolderPath;
    cardPath +=  "invisible.png";
    invisCard = new QPixmap(cardPath);

    cardPath = cardFolderPath;
    cardPath +=  "joker-1.png";
    jokerCard = new QPixmap(cardPath);

    for(unsigned i = 0; i < 4; i++) {
        for(unsigned j = 0; j < 13; j++) {
            cardPath = cardFolderPath;
            cardPath += symbols[i];
            cardPath += numbers[j];

            cardImages[i].push_back(QPixmap(cardPath));
        }
    }
}

void Widget::showLayouts() {
    ui->playerCardLayout->addWidget(playerLayouts[0]);
    ui->tableGridLayout->addWidget(tableLayout, 0, 1);

    auto style = QString("background-color: rgb(6,140,18); \
                            margin:5px; \
                            border:1px solid rgb(0, 0, 0); \
                            font-family: Arial");

    playerLayouts[0]->setStyleSheet(style);

    tableLayout->setStyleSheet(style);

    ui->otherPlayersLayout->addItem(new QSpacerItem(20, 20, QSizePolicy::Expanding), 0, 0);

    for(unsigned i = 1; i < maxPlayers; i++) {
        ui->otherPlayersLayout->addWidget(playerLayouts[i], 0, 2*i-1);
        playerLayouts[i]->setStyleSheet(style);
    }

    ui->otherPlayersLayout->addItem(new QSpacerItem(20, 20, QSizePolicy::Expanding), 0, 2*maxPlayers-2);
}

QString Widget::getName()
{
    QString name = ui->nameLineEdit->text().trimmed();
#ifdef DEBUG
    //static unsigned i = 0;
    //name.append(QString(" %1").arg(i++));
#endif
    return name;
}

void Widget::requestBetSlot(int callMoney)
{
    emit requestBetFromBetInput(callMoney, playerLayouts[0]->getMoney());
}

void Widget::returnBetSlot(int bet)
{
    emit returnBetToClient(bet);
}

void Widget::playerWon(int id) {
    unsigned index = getIndexOfId(id);

    if(index >= playerLayouts.size()) {
        qDebug() << "FILE: " << __FILE__ << ", LINE: " << __LINE__
                 << ", id (" << id << ") not found in " << __FUNCTION__;
        return;
    }

    QString msg;

    if(index == 0) {
        sm.playPlayerWon();
        msg = QString("You've won the round");
    }
    else {
        msg = QString("%1 has won a round.").arg(playerLayouts[index]->getName());
    }

    messagePlayer(msg);
}

void Widget::serverExited()
{
    resetGameBoard();
    ui->stackedWidget->setCurrentWidget(ui->beforeGameMenuWidget);
    showMessageBox("Server exited", "Server exited");
}

void Widget::hostTerminatedConnection()
{
    showMessageBox("Host connection terminated", "The connection to the host server has been lost.");

    ui->stackedWidget->setCurrentWidget(ui->beforeGameMenuWidget);
}

void Widget::gameStarted(int newPlayerCount, int localID, const QVector<Player> &players, int initialMoney) {
    sm.playGameStarted();
    qDebug() << "FILE:" << __FILE__ << ", LINE:" << __LINE__ << __FUNCTION__;
    messagePlayer(QString("Game started"));

    ui->playerCountPicker->setValue(newPlayerCount);

    playerLayouts[0]->setId(localID);

    for(unsigned i = 1, j = 0; i < playerCount && j < players.size(); i++, j++) {
        if(players[j].getID() == localID) {
            playerLayouts[j]->setName(getName());

            j++;
            if(j >= players.size()) break;
        }

        playerLayouts[i]->setId(players[j].getID());
        playerLayouts[i]->setName(players[j].getUsername());
    }

    for(unsigned i = qMin(playerCount, (unsigned)players.size()); i < maxPlayers; i++) {
        playerLayouts[i]->setId(lostID);
        playerLayouts[i]->setName("This player shouldn't be shown");
    }

    tableLayout->hideCards();

    for(unsigned i = 0; i < playerCount; i++) {
        playerLayouts[i]->setVisible(true);
        playerLayouts[i]->setMoney(initialMoney);
        playerLayouts[i]->setCardsToUnkown();
        playerLayouts[i]->setCardsToVisible(2);
    }

    for(unsigned i = playerCount; i < maxPlayers; i++) {
        playerLayouts[i]->setVisible(false);
        playerLayouts[i]->hideCards();
    }

    ui->betInput->setVisible(true);

    ui->stackedWidget->setCurrentWidget(ui->singleplayerMenuWidget);
}

void Widget::playerBetChanged(int id, int bet) {
    unsigned index = getIndexOfId(id);

    if (index >= playerLayouts.size()) {
        qDebug() << "FILE: " << __FILE__ << ", LINE: " << __LINE__
                 << ", given id not found in playerLayouts in " << __FUNCTION__
                 << "\n {id: " << id << ", size: " << playerLayouts.size()
                 << ", index: " << index << "}\n";
        return;
    }

    playerLayouts[index]->setBet(bet);
}

void Widget::potChanged(int pot) {
    if (tableLayout != nullptr) {
        tableLayout->setMoney(pot);
    }
    else {
        qDebug() << "FILE: " << __FILE__ << ", FUNCTION: " << __FUNCTION__ << ", LINE: " << __LINE__
                 << ", tableLayout should not be nullptr, there is a problem with initialization.";
    }
}

void Widget::tableCardsChanged(const std::vector<Card> &cards) {
    sm.playCardsChanged();

    if (tableLayout != nullptr) {
        qDebug() << "Table cards changed, size = " << cards.size();
        tableLayout->changeCards(cards);
    }
    else {
        qDebug() << "FILE: " << __FILE__ << ", FUNCTION: " << __FUNCTION__ << ", LINE: " << __LINE__
                 << ", tableLayout should not be nullptr, there is a problem with initialization.";
    }
}

void Widget::playerMoneyChanged(int id, int currentMoney) {
    unsigned index = getIndexOfId(id);

    if(index >= playerLayouts.size()) {
        qDebug() << "FILE: " << __FILE__ << ", LINE: " << __LINE__
                 << ", id (" << id << ") not found in " << __FUNCTION__;
        return;
    }

    playerLayouts[index]->setMoney(currentMoney);
}

void Widget::playerCardsChanged(int id, const std::vector<Card> &cards) {
    qDebug() << "FILE:" << __FILE__ << ", LINE:" << __LINE__ << __FUNCTION__;
    unsigned index = getIndexOfId(id);

    if(index >= playerLayouts.size()) {
        qDebug() << "FILE: " << __FILE__ << ", LINE: " << __LINE__
                 << ", id (" << id << ") not found in " << __FUNCTION__;
        return;
    }

    if(index != 0) {
        // sm.playShowCards();
    }

    playerLayouts[index]->changeCards(cards);
    qDebug() << " --- Player cards changed ID: " << id << ", my ID: "
             << playerLayouts[0]->getId() << " --- ";
}

void Widget::playerLost(int id) {
    qDebug() << "FILE:" << __FILE__ << ", LINE:" << __LINE__ << __FUNCTION__;
    unsigned index = getIndexOfId(id);

    if(index >= playerLayouts.size()) {
        qDebug() << "FILE: " << __FILE__ << ", LINE: " << __LINE__
                 << ", id (" << id << ") not found in " << __FUNCTION__;
        return;
    }

    auto loser = playerLayouts[index];

    if(index == 0) {
        showMessageBox("Player lost", "You've lost.");

        loser->hideCards();
        loser->setBet(0);
        loser->setMoney(0);
        loser->setId(lostID);

        ui->betInput->setHidden(true);
    }
    else {
        QString msg = QString("%1 is out.").arg(loser->getName());
        messagePlayer(msg);

        // This needs to change if showCards is also called
        loser->hideCards();
        loser->hide();
        loser->setBet(0);
        loser->setMoney(0);
        loser->setId(lostID);
    }
}

void Widget::playerFolded(int id) {
    int index = getIndexOfId(id);

    if(index >= playerLayouts.size()) {
        qDebug() << "FILE: " << __FILE__ << ", LINE: " << __LINE__
                 << ", id (" << id << ") not found in " << __FUNCTION__;
        return;
    }

    auto player = playerLayouts[index];
    QString msg = QString("%1 has folded.").arg(player->getName());
	player->hideCards();
    messagePlayer(msg);
}

void Widget::roundStarted() {
    // messagePlayer(QString("New round started..."));
    if(playerLayouts.size() == 0) {
        throw std::logic_error("Runda je pocela pre nego sto su se inicijalizovali layouts za igrace");
    }

    int myID = playerLayouts[0]->getId();
    for(auto p: playerLayouts) {
        if(p->isHidden() == false && p->getId() != myID) {
            p->setCardsToUnkown();
        }
    }
}

void Widget::resetGameBoard() {
    ui->joinButton->setEnabled(true);
    ui->hostButton->setEnabled(true);

    ui->serverPlayerCountLabel->setHidden(true);
    updateServerPlayerCountLabel(0);

    ui->startGameButton->setEnabled(false);
    ui->lonerGameButton->setEnabled(true);

    ui->bigBlindEdit->setEnabled(true);
    ui->initialMoneyEdit->setEnabled(true);

    deleteServer();

    for(unsigned i = 0; i < playerLayouts.size(); i++)
    {
        playerLayouts[i]->setId(lostID);
        playerLayouts[i]->setName("This player shouldn't be shown");
        playerLayouts[i]->setVisible(false);
        playerLayouts[i]->hideCards();
        playerLayouts[i]->setMoney(0);
        playerLayouts[i]->setBet(0);
    }

    tableLayout->hideCards();

    messagePlayer(QString(""));
}

void Widget::gameEnded() {
    QString title, msgText;

    if(playerLayouts[0]->getId() != lostID) {
        title = QString("Game Ended in Victory");
        msgText = QString("You won, congratulations! Money made: %1").arg(playerLayouts[0]->getMoney());
    }
    else {
        title = QString("Game Ended in Loss");
        msgText = QString("The game ended. You've lost.");
    }

    showMessageBox(title, msgText);

    ui->stackedWidget->setCurrentWidget(ui->mainMenuWidget);

    resetGameBoard();
}

void Widget::on_singleplayerButton_clicked()
{
    ui->stackedWidget->setCurrentWidget(ui->beforeGameMenuWidget);
}

void Widget::on_exitButton_clicked()
{
    QCoreApplication::quit();
}

void Widget::on_optionsButton_clicked()
{
    ui->stackedWidget->setCurrentWidget(ui->optionsMenuWidget);

    if(ui->exampleLabel->text().length() > 0) {
        ui->exampleLabel->setText("");
        ui->exampleLabel->setPixmap(jokerCard->scaledToHeight(cardHeight, Qt::SmoothTransformation));
    }
}

void Widget::on_optionsBackButton_clicked()
{
    ui->stackedWidget->setCurrentWidget(ui->mainMenuWidget);
}

void Widget::on_spBack_clicked()
{
    emit playerQuitSignal();

    ui->stackedWidget->setCurrentWidget(ui->mainMenuWidget);

    resetGameBoard();
}

void delay(unsigned ms = 100) {
    QTime dieTime= QTime::currentTime().addMSecs(ms);
    while (QTime::currentTime() < dieTime)
        QCoreApplication::processEvents(QEventLoop::AllEvents, 50);
}

void Widget::on_startGameButton_clicked()
{
    if(sc != nullptr) {
        unsigned botsCount = playerCount;
        const unsigned playersConnected = sc->getPlayerCount();

        if(botsCount >= playersConnected) {
            botsCount -= playersConnected;
        }
        else {
            botsCount = 0;
        }

        sc->startGame(botsCount);
    }
    else {
        qDebug() << __FILE__ << " line: " << __LINE__ << " - server is nullptr.";
    }
}

void Widget::on_lonerGameButton_clicked()
{
    on_hostButton_clicked();
    delay();
    on_startGameButton_clicked();
}

void Widget::on_playerCountPicker_valueChanged(int numberPicked)
{
    playerCount = numberPicked;
}

void Widget::on_backToStartButton_clicked()
{
    ui->stackedWidget->setCurrentWidget(ui->mainMenuWidget);
}

void Widget::on_hostButton_clicked()
{
    deleteServer();

    unsigned initialMoney, bigBlind;

    initialMoney = ui->initialMoneyEdit->text().toInt();
    if(initialMoney < 500) initialMoney = 500;

    bigBlind = ui->bigBlindEdit->text().toInt();
    if(bigBlind < 10) bigBlind = 500;
    if(bigBlind > initialMoney / 10) bigBlind = initialMoney / 10;

    sc = new ServerController(nullptr, initialMoney, bigBlind);

    connect(sc, &ServerController::windowClosed, this, &Widget::serverExited);
    connect(sc, &ServerController::playerCountChanged, this, &Widget::updateServerPlayerCountLabel);

    sc->toggleStartServer();

    ui->serverPlayerCountLabel->setHidden(false);
    updateServerPlayerCountLabel(0);

    ui->startGameButton->setEnabled(true);
    ui->lonerGameButton->setEnabled(false);

    ui->joinButton->setEnabled(false);
    ui->hostButton->setEnabled(false);

    ui->bigBlindEdit->setEnabled(false);
    ui->initialMoneyEdit->setEnabled(false);

    emit attemptConnectionToAddress(localAddress);
}

void Widget::updateServerPlayerCountLabel(unsigned count) {
    sm.playPlayerCountChanged();

    ui->serverPlayerCountLabel->setText(QString("Players connected to server: %1").arg(count));
}

void Widget::on_saveNameButton_clicked()
{
    QString name = ui->nameLineEdit->text().trimmed();

    if(name.size() <= 0) {
        ui->nameLineEdit->setText(settings.value(QString("Name"), QString("Player")).toString());

        showMessageBox("Invalid name", "Your username must not be made up of only whitespace.");

        return ;
    }

    settings.setValue(QString("Name"), name);
    settings.sync();
}

void Widget::on_joinButton_clicked()
{
    emit attemptConnection();
    qDebug() << "attemptConnection";
}

void Widget::on_musicSlider_valueChanged(int value)
{
    settings.setValue(QString("Music Volume"), value);
    settings.sync();
}

void Widget::on_soundSlider_valueChanged(int value)
{
    settings.setValue(QString("Sound Volume"), value);
    settings.sync();

    sm.setVolume(value);
}

void Widget::on_cardHeightEdit_valueChanged(int height)
{
    settings.setValue(QString("Card Height"), height);
    settings.sync();

    cardHeight = height;
    if(jokerCard != nullptr) ui->exampleLabel->setPixmap(jokerCard->scaledToHeight(cardHeight, Qt::SmoothTransformation));

    CardLayout::setCardHeight(cardHeight);
}

void Widget::on_Widget_destroyed()
{
    emit playerQuitSignal();
    qDebug() << "FILE: " << __FILE__ << ", FUNCTION: " << __FUNCTION__
             << "\n Window closed ";
}
