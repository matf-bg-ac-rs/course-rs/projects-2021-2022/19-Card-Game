#include "catch.hpp"
#include "client/cardnumber.cpp"

#include <vector>
#include <algorithm>

TEST_CASE( "Konvertovanje CardNumber u int", "[CardNumber][Card]" ) {
    // Arrange
    std::vector<CardNumber> in{CardNumber::Two, CardNumber::Ace, CardNumber::Ten, CardNumber::Five};
    std::vector<int> out(in.size());

    // Act
    std::transform(in.begin(), in.end(), out.begin(), [](CardNumber cn) -> int { return cn.toInt(); });

    // Assert
    CHECK( out[0] == 2 );
    CHECK( out[1] == 1 );
    CHECK( out[2] == 10 );
    REQUIRE( out[3] == 5 );
}

TEST_CASE( "Konvertovanje int u CardNumber", "[CardNumber][Card]" ) {
    SECTION("Validne konverzije") {
        std::vector<int> input{2, 1, 10, 5};
        std::vector<CardNumber> output{};
        std::vector<CardNumber> expectedOutput{CardNumber::Two, CardNumber::Ace, CardNumber::Ten, CardNumber::Five};
        std::vector<bool> res{};

        for(unsigned i = 0; i < input.size(); i++) {
            output.push_back(CardNumber::intToNumber(input[i]));
        }

        for(unsigned i = 0; i < output.size(); i++) {
            res.push_back(expectedOutput[i] == output[i]);
        }

        REQUIRE( expectedOutput.size() == output.size() );
        CHECK( res[0] );
        CHECK( res[1] );
        CHECK( res[2] );
        REQUIRE( res[3] );
    }

    SECTION("Nevalidne konverzije") {
        std::vector<int> input{-1, 0, 16};

        // Sta treba da se desi? Exception, jel da?
        CHECK_THROWS( CardNumber::intToNumber(input[0]) );
        CHECK_THROWS( CardNumber::intToNumber(input[1]) );
        REQUIRE_THROWS( CardNumber::intToNumber(input[2]) );
    }
}

TEST_CASE( "Konvertovanje CardNumber u QString", "[CardNumber][Card]" ) {
    std::vector<CardNumber> in{CardNumber::Two, CardNumber::Ace, CardNumber::Ten, CardNumber::Five};
    std::vector<QString> out(in.size());

    std::transform(in.begin(), in.end(), out.begin(), [](CardNumber cn) -> QString { return CardNumber::numberToString(cn); });

    CHECK( out[0].compare(QString("Two")) == 0 );
    CHECK( out[1].compare(QString("Ace")) == 0 );
    CHECK( out[2].compare(QString("Ten")) == 0 );
    REQUIRE( out[3].compare(QString("Five")) == 0 );
}

TEST_CASE( "Konvertovanje QString u CardNumber", "[CardNumber][Card]" ) {
    SECTION("Validne konverzije") {
        std::vector<QString> input{"Two", "Ace", "ten", "FIVE"};
        std::vector<CardNumber> output{};
        std::vector<CardNumber> expectedOutput{CardNumber::Two, CardNumber::Ace, CardNumber::Ten, CardNumber::Five};
        std::vector<bool> res{};

        for(unsigned i = 0; i < input.size(); i++) {
            output.push_back(CardNumber::stringToNumber(input[i]));
        }

        for(unsigned i = 0; i < output.size(); i++) {
            res.push_back(expectedOutput[i] == output[i]);
        }

        REQUIRE( expectedOutput.size() == output.size() );
        CHECK( res[0] );
        CHECK( res[1] );
        CHECK( res[2] );
        REQUIRE( res[3] );
    }

    SECTION("Nevalidne konverzije") {
        std::vector<QString> inputStrings{"Tw0", "Aceeee", "10", "f!v3"};

        // Sta treba da se desi? Exception, jel da?
        CHECK_THROWS( CardNumber::stringToNumber(inputStrings[0]) );
        CHECK_THROWS( CardNumber::stringToNumber(inputStrings[1]) );
        CHECK_THROWS( CardNumber::stringToNumber(inputStrings[2]) );
        REQUIRE_THROWS( CardNumber::stringToNumber(inputStrings[3]) );
    }
}

TEST_CASE( "Poredjenje 2 CardNumber-a", "[CardNumber][Card]" ) {
    SECTION("jednakost") {
        std::vector<CardNumber> in1{CardNumber::Two, CardNumber::Ace, CardNumber::Ten, CardNumber::Five};
        std::vector<CardNumber> in2{CardNumber::Two, CardNumber::Ace, CardNumber::King, CardNumber::Six};
        std::vector<bool> res{};

        for(unsigned i = 0; i < in1.size(); i++) {
            res.push_back(in1[i] == in2[i]);
        }

        CHECK( res[0] );
        CHECK( res[1] );
        CHECK_FALSE( res[2] );
        REQUIRE_FALSE( res[3] );
    }

    SECTION("nejednakost") {
        std::vector<CardNumber> in1{CardNumber::Two, CardNumber::Ace, CardNumber::Ten, CardNumber::Five};
        std::vector<CardNumber> in2{CardNumber::Two, CardNumber::Ace, CardNumber::King, CardNumber::Six};
        std::vector<bool> res{};

        for(unsigned i = 0; i < in1.size(); i++) {
            res.push_back(in1[i] != in2[i]);
        }

        CHECK_FALSE( res[0] );
        CHECK_FALSE( res[1] );
        CHECK( res[2] );
        REQUIRE( res[3] );
    }

    SECTION("vece") {
        std::vector<CardNumber> in1{CardNumber::Four, CardNumber::Ace, CardNumber::King, CardNumber::Five};
        std::vector<CardNumber> in2{CardNumber::Three, CardNumber::Jack, CardNumber::King, CardNumber::Six};
        std::vector<bool> res{};

        for(unsigned i = 0; i < in1.size(); i++) {
            res.push_back(in1[i] > in2[i]);
        }

        CHECK( res[0] );
        CHECK( res[1] );
        CHECK_FALSE( res[2] );
        REQUIRE_FALSE( res[3] );
    }

    SECTION("manje") {
        std::vector<CardNumber> in1{CardNumber::Four, CardNumber::Ace, CardNumber::King, CardNumber::Five};
        std::vector<CardNumber> in2{CardNumber::Three, CardNumber::Jack, CardNumber::King, CardNumber::Six};
        std::vector<bool> res{};

        for(unsigned i = 0; i < in1.size(); i++) {
            res.push_back(in1[i] < in2[i]);
        }

        CHECK_FALSE( res[0] );
        CHECK_FALSE( res[1] );
        CHECK_FALSE( res[2] );
        REQUIRE( res[3] );
    }
}

TEST_CASE( "Sabiranje i oduzimanjeCardNumber-a", "[CardNumber][Card]" ) {
    SECTION("Sabiranje") {
        std::vector<CardNumber> in{CardNumber::Two, CardNumber::Two, CardNumber::Five};
        CardNumber inForThrow(CardNumber::King);
        std::vector<CardNumber> output{};
        std::vector<CardNumber> expectedOutput{CardNumber::Two, CardNumber::Three, CardNumber::Seven};
        std::vector<bool> res{};

        for(unsigned i = 0; i < in.size(); i++) {
            output.push_back(in[i] + i);
        }

        for(unsigned i = 0; i < in.size(); i++) {
            res.push_back(expectedOutput[i] == output[i]);
        }

        // res se koristi jer baca gresku
        // kad se stavi expectedOutput[0] == output[0]
        CHECK( res[0] );
        CHECK( res[1] );
        CHECK( res[2] );
        REQUIRE_THROWS( inForThrow + 2 );
    }

    SECTION("Oduzimanje") {
        std::vector<CardNumber> in{CardNumber::Three, CardNumber::Ten, CardNumber::Jack};
        CardNumber inForThrow(CardNumber::Two);
        std::vector<CardNumber> output{};
        std::vector<CardNumber> expectedOutput{CardNumber::Three, CardNumber::Nine, CardNumber::Nine};
        std::vector<bool> res{};

        for(unsigned i = 0; i < in.size(); i++) {
            output.push_back(in[i] - i);
        }

        for(unsigned i = 0; i < in.size(); i++) {
            res.push_back(expectedOutput[i] == output[i]);
        }

        // res se koristi jer baca gresku
        // kad se stavi expectedOutput[0] == output[0]
        CHECK( res[0] );
        CHECK( res[1] );
        CHECK( res[2] );
        REQUIRE_THROWS( inForThrow - 4 );
    }
}
