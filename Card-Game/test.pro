QT += core network gui widgets multimedia

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++17

isEmpty(CATCH_INCLUDE_DIR): CATCH_INCLUDE_DIR=$$(CATCH_INCLUDE_DIR)
!isEmpty(CATCH_INCLUDE_DIR): INCLUDEPATH *= $${CATCH_INCLUDE_DIR}

isEmpty(CATCH_INCLUDE_DIR): {
    message("CATCH_INCLUDE_DIR is not set, assuming Catch2 can be found automatically in your system")
}

SOURCES += \
	client/card.cpp \
	client/cardhand.cpp \
	client/cardnumber.cpp \
	client/cardsymbol.cpp \
	client/clientcontroller.cpp \
	client/gameclient.cpp \
	client/player.cpp \
	server/gameserver.cpp \
	server/servercontroller.cpp \
	server/serverworker.cpp \
	mainTest.cpp \
	tests/cardNumberTests.cpp \
	tests/playerTests.cpp \
	ui/layouts/betinput.cpp \
	ui/layouts/cardlayout.cpp \
	ui/layouts/playerlayout.cpp \
	ui/layouts/tablelayout.cpp \
	ui/soundmanager.cpp \
	ui/widget.cpp
	
HEADERS += \
	client/card.hpp \
	client/cardhand.hpp \
	client/cardnumber.hpp \
	client/cardsymbol.hpp \
	client/clientcontroller.h \
	client/gameclient.h \
	client/player.hpp \
	server/gameserver.h \
	server/servercontroller.h \
	server/serverworker.h \
	catch.hpp \
	ui/layouts/betinput.hpp \
	ui/layouts/cardlayout.hpp \
	ui/layouts/playerlayout.hpp \
	ui/layouts/tablelayout.hpp \
	ui/soundmanager.h \
	ui/widget.h

RESOURCES += \
    resources.qrc

FORMS += \
    client/clientcontroller.ui \
    server/servercontroller.ui \
    ui/layouts/betinput.ui \
    ui/widget.ui

DISTFILES += \
    font/CoffeetinInitials-YXJ2.ttf \
    images/512x512_Dissolve_Noise_Texture 20% opacity.png \
    images/512x512_Dissolve_Noise_Texture 3,7% opacity.png \
    images/512x512_Dissolve_Noise_Texture 5% opacity.png \
    images/512x512_Dissolve_Noise_Texture 7,5% opacity.png \
    images/512x512_Dissolve_Noise_Texture one and half percent opacity.png \
    images/512x512_Dissolve_Noise_Texture.png \
    images/cards/back.png \
    images/cards/club-1.png \
    images/cards/club-10.png \
    images/cards/club-12.png \
    images/cards/club-13.png \
    images/cards/club-14.png \
    images/cards/club-2.png \
    images/cards/club-3.png \
    images/cards/club-4.png \
    images/cards/club-5.png \
    images/cards/club-6.png \
    images/cards/club-7.png \
    images/cards/club-8.png \
    images/cards/club-9.png \
    images/cards/diamond-1.png \
    images/cards/diamond-10.png \
    images/cards/diamond-12.png \
    images/cards/diamond-13.png \
    images/cards/diamond-14.png \
    images/cards/diamond-2.png \
    images/cards/diamond-3.png \
    images/cards/diamond-4.png \
    images/cards/diamond-5.png \
    images/cards/diamond-6.png \
    images/cards/diamond-7.png \
    images/cards/diamond-8.png \
    images/cards/diamond-9.png \
    images/cards/heart-1.png \
    images/cards/heart-10.png \
    images/cards/heart-12.png \
    images/cards/heart-13.png \
    images/cards/heart-14.png \
    images/cards/heart-2.png \
    images/cards/heart-3.png \
    images/cards/heart-4.png \
    images/cards/heart-5.png \
    images/cards/heart-6.png \
    images/cards/heart-7.png \
    images/cards/heart-8.png \
    images/cards/heart-9.png \
    images/cards/invisible.png \
    images/cards/joker-1.png \
    images/cards/joker-2.png \
    images/cards/spade-1.png \
    images/cards/spade-10.png \
    images/cards/spade-12.png \
    images/cards/spade-13.png \
    images/cards/spade-14.png \
    images/cards/spade-2.png \
    images/cards/spade-3.png \
    images/cards/spade-4.png \
    images/cards/spade-5.png \
    images/cards/spade-6.png \
    images/cards/spade-7.png \
    images/cards/spade-8.png \
    images/cards/spade-9.png \
    sound/145434__soughtaftersounds__old-music-box-1.mp3 \
    sound/145434__soughtaftersounds__old-music-box-1.wav \
    sound/171697__nenadsimic__menu-selection-click.wav \
    sound/240776__f4ngy__card-flip.wav \
    sound/246420__waveplaysfx__media-game-sound-sfx-short-generic-menu.wav \
    sound/246420__waveplaysfx__media-game-sound-sfx-short-generic-menu1.wav \
    sound/256455__deleted-user-4772965__mouse-click.wav \
    sound/26777__junggle__btn402.mp3 \
    sound/26777__junggle__btn402.wav \
    sound/277033__headphaze__ui-completed-status-alert-notification-sfx001.wav \
    sound/366102__original-sound__confirmation-upward.wav \
    widget.qss
