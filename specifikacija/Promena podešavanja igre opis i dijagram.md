# Promena podešavanja igre

**Kratak opis**: 
Igrač podešava jačinu zvuka, muzike, i svoje ime. Ime se koristi u slučaju da igra u multiplejer partiji.

**Akteri**:
Igrač

**Preduslovi**: 
Igra je pokrenuta, i nalazi se u Settings meniju.

**Postuslovi**: 
Izvršena su željena podešavanja.

**Osnovni tok**:
1. Igrač klikće na polje za unos teksta, i menja napisan tekst.
2. Igrač klikće na dugme za čuvanje imena.

**Alternativni tokovi**: 
1. Igrač pomera slajder dok nije zadovoljan jačinom zvuka efekta. 
2. Igrač pomera slajder dok nije zadovoljan jačinom muzike. 

**Podtokovi**: /

**Specijalni zahtevi**: /

**Dodatne informacije**: /
