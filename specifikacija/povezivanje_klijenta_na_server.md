# Povezivanje klijenta na server

**Kratak opis**: Klikom na dugme, klijent šalje serveru zahtev za povezivanje preko TCP protokola, nakon uspešnog povezivanja klijent unosi korisničko ime i otvara se glavni meni.

**Akteri**: Klijent.

**Preduslovi**: /

**Postuslovi**:Igrač je povezan na server sa izabranim korisničkim imenom i otvoren je glavni meni.

**Osnovni tok**:
1. Klijent šalje serveru TCP zahtev za povezivanje pritiskom na dugme.
2. Server odobrava taj zahtev, uspostavljajući TCP konekciju.
3. Igrač piše ime u dato polje i šalje ga serveru pritiskom na dugme.
4. Server odobrava izbor pod uslovom da ime nije već izabrano.
5. Otvara se glavni meni.

**Alternativni tokovi**: <br />
A1: Prikazuje se prozor sa porukom o neuspešnom povezivanju i dugmetom za ponovni pokušaj uspostavljanja konekcije.
A2: Prikazuje se prozor sa porukom da je ime već u upotrebi i poljem za ponovni unos imena.

**Podtokovi**: /

**Specijalni zahtevi**: <br />
Svaki od igrača mora biti povezan na internet i posedovati kod sebe klijent aplikacije. Server aplikacije mora biti pokrenut u trenutku zahtevanja konekcije.

**Dodatne informacije**: /


